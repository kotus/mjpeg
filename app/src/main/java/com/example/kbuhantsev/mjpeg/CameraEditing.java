package com.example.kbuhantsev.mjpeg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.kbuhantsev.mjpeg.pojo.Camera;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CameraEditing extends AppCompatActivity implements View.OnClickListener{

    public EditText mEditTextName, mEditTextURL, mEditTextLogin, mEditTextPassword, mEditEditTextPreviewPath,
            mEditTextUpCommand, mEditTextDownCommand, mEditTextLeftCommand, mEditTextRightCommand, mEditTextHomeCommand;
    public Button mButtonSaveCamera, mButtonAddAction;
    public ListView mListView;

    private Camera mCamera;
    private Integer mPosition;

    private SimpleAdapter mAdapter;
    private ArrayList<Map<String, Object>> mData;
    private Map<String, Object> mItem;

    // имена атрибутов для Map
    final String ATTRIBUTE_NAME_TEXT = "ATTRIBUTE_NAME_TEXT";
    final String ATTRIBUTE_NAME_COMMAND = "ATTRIBUTE_NAME_COMMAND";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_editing);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mEditTextName = (EditText) findViewById(R.id.editTextName);
        mEditTextURL = (EditText) findViewById(R.id.editTextURL);
        mEditTextLogin = (EditText) findViewById(R.id.editTextLogin);
        mEditTextPassword = (EditText) findViewById(R.id.editTextPassword);
        mEditEditTextPreviewPath = (EditText) findViewById(R.id.editTextPreviewPath);
        mEditTextUpCommand = (EditText) findViewById(R.id.editTextUpCommand);
        mEditTextLeftCommand = (EditText) findViewById(R.id.editTextLeftCommand);
        mEditTextRightCommand = (EditText) findViewById(R.id.editTextRightCommand);
        mEditTextDownCommand = (EditText) findViewById(R.id.editTextDownCommand);
        mEditTextHomeCommand = (EditText) findViewById(R.id.editTextHomeCommand);

        mButtonSaveCamera = (Button) findViewById(R.id.buttonSaveCamera);
        mButtonAddAction = (Button) findViewById(R.id.buttonAdd);

        mButtonSaveCamera.setOnClickListener(this);
        mButtonAddAction.setOnClickListener(this);

        mListView = (ListView) findViewById(R.id.listView);
        mData = new ArrayList<Map<String, Object>>();
        String[] from = {ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_COMMAND};
        int[] to = {R.id.tvItemName, R.id.tvItemCommand};
        mAdapter = new SimpleAdapter(this, mData, R.layout.list_item, from, to);
        mListView.setAdapter(mAdapter);

        Intent intent = getIntent();
        Integer requestCode = intent.getIntExtra("requestCode", -1);
        if (requestCode == 1){ //REQUEST_CODE_EDIT

            Bundle bundle = getIntent().getParcelableExtra(Camera.class.getCanonicalName());
            mCamera = (Camera) bundle.get("camera");
            mPosition = getIntent().getIntExtra("position", -1);
            if (mCamera != null){
                mEditTextName.setText(mCamera.getName());
                mEditTextURL.setText(mCamera.getUrl());
                mEditTextLogin.setText(mCamera.getLogin());
                mEditTextPassword.setText(mCamera.getPassword());
                mEditEditTextPreviewPath.setText(mCamera.getPreviewPath());

                mEditTextUpCommand.setText(mCamera.getUpCommand());
                mEditTextDownCommand.setText(mCamera.getDownCommand());
                mEditTextLeftCommand.setText(mCamera.getLeftCommand());
                mEditTextRightCommand.setText(mCamera.getRightCommand());
                mEditTextHomeCommand.setText(mCamera.getHomeCommand());

                mData.addAll(mCamera.getCommands());
                mAdapter.notifyDataSetChanged();

            }

        } else if (requestCode == 2) {//REQUEST_CODE_CREATE


        }



    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonSaveCamera:
                if (mCamera == null){
                    mCamera = new Camera(mEditTextURL.getText().toString(),
                            mEditEditTextPreviewPath.getText().toString(),
                            mEditTextName.getText().toString(),
                            mEditTextLogin.getText().toString(),
                            mEditTextPassword.getText().toString(),
                            mData);
                    mCamera.setUpCommand(mEditTextUpCommand.getText().toString());
                    mCamera.setDownCommand(mEditTextDownCommand.getText().toString());
                    mCamera.setLeftCommand(mEditTextLeftCommand.getText().toString());
                    mCamera.setRightCommand(mEditTextRightCommand.getText().toString());
                    mCamera.setHomeCommand(mEditTextHomeCommand.getText().toString());
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("camera", mCamera);
                    intent.putExtra(Camera.class.getCanonicalName(), bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    mCamera.setUrl(mEditTextURL.getText().toString());
                    mCamera.setName(mEditTextName.getText().toString());
                    mCamera.setLogin(mEditTextLogin.getText().toString());
                    mCamera.setPassword(mEditTextPassword.getText().toString());
                    mCamera.setPreviewPath(mEditEditTextPreviewPath.getText().toString());
                    mCamera.setCommands(mData);

                    mCamera.setUpCommand(mEditTextUpCommand.getText().toString());
                    mCamera.setDownCommand(mEditTextDownCommand.getText().toString());
                    mCamera.setLeftCommand(mEditTextLeftCommand.getText().toString());
                    mCamera.setRightCommand(mEditTextRightCommand.getText().toString());
                    mCamera.setHomeCommand(mEditTextHomeCommand.getText().toString());

                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("camera", mCamera);
                    intent.putExtra(Camera.class.getCanonicalName(), bundle);
                    intent.putExtra("position", mPosition);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.buttonAdd:

                //TODO
                //Модальное окно для добавления доп комманд
                mItem = new HashMap<String, Object>();
                mItem.put(ATTRIBUTE_NAME_TEXT, "up");
                mItem.put(ATTRIBUTE_NAME_COMMAND, "sfdsfdsf");
                mData.add(mItem);
                mAdapter.notifyDataSetChanged();

       }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
