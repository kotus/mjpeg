package com.example.kbuhantsev.mjpeg.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by k.buhantsev on 19.04.2016.
 */
public class Camera implements Parcelable{

    String url;
    String previewPath;
    String name;
    String login;
    String password;
    ArrayList<Map<String, Object>> commands = new ArrayList<Map<String, Object>>();
    String upCommand;
    String downCommand;
    String leftCommand;
    String rightCommand;
    String homeCommand;

    public String getUpCommand() {
        return upCommand;
    }

    public void setUpCommand(String upCommand) {
        this.upCommand = upCommand;
    }

    public String getDownCommand() {
        return downCommand;
    }

    public void setDownCommand(String downCommand) {
        this.downCommand = downCommand;
    }

    public String getLeftCommand() {
        return leftCommand;
    }

    public void setLeftCommand(String leftCommand) {
        this.leftCommand = leftCommand;
    }

    public String getRightCommand() {
        return rightCommand;
    }

    public void setRightCommand(String rightCommand) {
        this.rightCommand = rightCommand;
    }

    public String getHomeCommand() {
        return homeCommand;
    }

    public void setHomeCommand(String homeCommand) {
        this.homeCommand = homeCommand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Camera camera = (Camera) o;

        if (!url.equals(camera.url)) return false;
        return name.equals(camera.name);

    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    public String getPreviewPath() {
        return previewPath;
    }

    public void setPreviewPath(String previewPath) {
        this.previewPath = previewPath;
    }

    public ArrayList<Map<String, Object>> getCommands() {
        return commands;
    }

    public void setCommands(ArrayList<Map<String, Object>> commands) {
        this.commands = commands;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Camera(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public Camera(String url, String previewPath, String name, String login, String password, ArrayList<Map<String, Object>> commands) {
        this.url = url;
        this.name = name;
        this.login = login;
        this.password = password;
        this.commands = commands;
        this.previewPath = previewPath;
    }

    @Override
    public String toString() {
        return name;
    }

    //Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(login);
        dest.writeString(password);
        dest.writeSerializable(commands);
        dest.writeString(previewPath);
        dest.writeString(upCommand);
        dest.writeString(downCommand);
        dest.writeString(leftCommand);
        dest.writeString(rightCommand);
        dest.writeString(homeCommand);

    }

    protected Camera(Parcel in) {

        url = in.readString();
        name = in.readString();
        login = in.readString();
        password = in.readString();
        commands = (ArrayList<Map<String,Object>>) in.readSerializable();
        previewPath = in.readString();
        upCommand = in.readString();
        downCommand = in.readString();
        leftCommand = in.readString();
        rightCommand = in.readString();
        homeCommand = in.readString();

    }

    public static final Creator<Camera> CREATOR = new Creator<Camera>() {

        @Override
        public Camera createFromParcel(Parcel in) {
            return new Camera(in);
        }

        @Override
        public Camera[] newArray(int size) {
            return new Camera[size];
        }

    };

}
