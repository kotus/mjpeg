package com.example.kbuhantsev.mjpeg;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.kbuhantsev.mjpeg.adapters.ItemClickSupport;
import com.example.kbuhantsev.mjpeg.adapters.RecyclerAdapter;
import com.example.kbuhantsev.mjpeg.pojo.Camera;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "myLogs";
    private static final Integer REQUEST_CODE_EDIT = 1;
    private static final Integer REQUEST_CODE_CREATE = 2;
    private Type dataListType = new TypeToken<ArrayList<Camera>>(){}.getType();

    public ArrayList<Camera> mCameras = new ArrayList<Camera>();
    private RecyclerView mRecyclerView;
    public final String CAMERAS_PREFERENCEC_NAME = "CAMERAS_LIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(CAMERAS_PREFERENCEC_NAME, Context.MODE_PRIVATE);

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String text = preferences.getString(CAMERAS_PREFERENCEC_NAME, "");
        ArrayList<Camera> cameraArrayList = gson.fromJson(text, dataListType);
        if (cameraArrayList != null) {
            Log.d(TAG, "cameraArrayList != null");
            mCameras.addAll(cameraArrayList);
        } else {
            Log.d(TAG, "cameraArrayList = null");
        }

        Log.d(TAG, "mCameras lenght = " + mCameras.size());

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(new RecyclerAdapter(mCameras));
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("Camera", mCameras.get(position));
                intent.putExtra(Camera.class.getCanonicalName(), bundle);
                startActivity(intent);
            }
        });
        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(final RecyclerView recyclerView, final int position, final View view) {

                PopupMenu popup = new PopupMenu(MainActivity.this, view, Gravity.CENTER);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit:
                                Intent intent = new Intent(MainActivity.this, CameraEditing.class);
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("camera", mCameras.get(position));
                                intent.putExtra(Camera.class.getCanonicalName(), bundle);
                                intent.putExtra("position", position);
                                intent.putExtra("requestCode", REQUEST_CODE_EDIT);
                                startActivityForResult(intent, REQUEST_CODE_EDIT);
                                Log.d(TAG, "onPopupMenuItemClick edit");
                                return true;
                            case R.id.delete:
                                mCameras.remove(position);
                                recyclerView.getAdapter().notifyItemRemoved(position);
                                Log.d(TAG, "onPopupMenuItemClick delete");
                                return true;
                            default:
                                Log.d(TAG, "onPopupMenuItemClick default");
                                return false;
                        }
                    }
                });

                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popup, popup.getMenu());
                popup.show();

                Log.d(TAG, "onItemLongClicked");
                return false;

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.about_app:
                Toast.makeText(this, "about_app", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.add_app:
                Intent intent = new Intent(MainActivity.this, CameraEditing.class);
                intent.putExtra("requestCode", REQUEST_CODE_CREATE);
                startActivityForResult(intent, REQUEST_CODE_CREATE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null) {

            return;

        } else {

            if (requestCode == REQUEST_CODE_EDIT) {

                Bundle bundle = data.getParcelableExtra(Camera.class.getCanonicalName());
                Camera camera = (Camera) bundle.get("camera");
                Integer position = data.getIntExtra("position", -1);
                mCameras.set(position, camera);
                mRecyclerView.getAdapter().notifyDataSetChanged();


            } else if (requestCode == REQUEST_CODE_CREATE){

                Bundle bundle = data.getParcelableExtra(Camera.class.getCanonicalName());
                mCameras.add((Camera) bundle.get("camera"));
                mRecyclerView.getAdapter().notifyDataSetChanged();

            }

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity: onDestroy()");
        SharedPreferences preferences = getSharedPreferences(CAMERAS_PREFERENCEC_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        preferencesEditor.putString(CAMERAS_PREFERENCEC_NAME, gson.toJson(mCameras, dataListType)).commit();
    }

}
