package com.example.kbuhantsev.mjpeg.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kbuhantsev.mjpeg.MainActivity;
import com.example.kbuhantsev.mjpeg.pojo.Camera;
import com.example.kbuhantsev.mjpeg.R;

import java.util.ArrayList;

/**
 * Created by k.buhantsev on 19.04.2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<Camera> mCameras;

    public RecyclerAdapter(ArrayList<Camera> mCameras) {
        this.mCameras = mCameras;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextViewName;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextViewName = (TextView) itemView.findViewById(R.id.recycler_view_name);
        }

        public void bind(final Camera item) {
            mTextViewName.setText(item.getName());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mCameras.get(position));
    }

    @Override
    public int getItemCount() {
        return mCameras.size();
    }

}
