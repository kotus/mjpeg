package com.example.kbuhantsev.mjpeg.pojo;

import android.os.AsyncTask;
import android.util.Base64;

import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by k.buhantsev on 15.04.2016.
 */
public class MovingCamera extends AsyncTask<String, Void, Boolean>{

    private String loginPassword;
    private String url;
    private String urlPath;
    private HashMap<String, String> parametrs;

    public MovingCamera(String url, String loginPassword, String urlPath, HashMap<String, String> parametrs) {
        this.loginPassword = loginPassword;
        this.url = url;
        this.urlPath = urlPath;
        this.parametrs = parametrs;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }

    @Override
    protected Boolean doInBackground(String... params) {

        String encoded = new String(Base64.encode(loginPassword.getBytes(), Base64.NO_WRAP));
        HttpURLConnection connection = null;

        try {

            URIBuilder builder = new URIBuilder();
            builder.setScheme("http")
                    .setHost(url)
                    .setPath(urlPath);
            for (Map.Entry<String, String> entry : parametrs.entrySet() ) {
                builder.addParameter(entry.getKey(), entry.getValue());
            }
            builder.build();

            URL urlObject = new URL(builder.toString());
            // "http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_home&lang=eng"
            connection = (HttpURLConnection) urlObject.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.setRequestProperty("Authorization", "Basic " + encoded);
            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                return true;
            }
            else {
                return false;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (connection != null){
                connection.disconnect();
            }

        }

    }

}
