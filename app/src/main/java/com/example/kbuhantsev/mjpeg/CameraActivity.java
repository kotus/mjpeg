package com.example.kbuhantsev.mjpeg;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.example.kbuhantsev.mjpeg.pojo.Camera;
import com.example.kbuhantsev.mjpeg.pojo.MovingCamera;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class CameraActivity extends AppCompatActivity implements View.OnClickListener{

    private WebView webView;
    private Button btnUp, btnDown, btnHome, btnLeft, btnRight;

    private Camera mCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        webView = (WebView) findViewById(R.id.webView);
        btnUp = (Button) findViewById(R.id.btnUp); //http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_up&lang=eng
        btnDown = (Button) findViewById(R.id.btnDown); //http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_down&lang=eng
        btnHome = (Button) findViewById(R.id.btnHome); //http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_home&lang=eng
        btnLeft = (Button) findViewById(R.id.btnLeft); //http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_left&lang=eng
        btnRight = (Button) findViewById(R.id.btnRight); //http://212.90.185.94:800/cgi-bin/action?action=cam_mv&diretion=cam_right&lang=eng

        Bundle bundle = getIntent().getParcelableExtra(Camera.class.getCanonicalName());
        mCamera = (Camera) bundle.get("Camera");

        webView.setOverScrollMode(WebView.SCREEN_STATE_OFF);
        webView.setClickable(true);

        URIBuilder builder = new URIBuilder();
        builder.setScheme("http");
        if ( !mCamera.getLogin().isEmpty() && !mCamera.getPassword().isEmpty()) {
            builder.setUserInfo(mCamera.getLogin() + ":" + mCamera.getPassword());
        }
        builder.setHost(mCamera.getUrl())
               .setPath(mCamera.getPreviewPath());
        try {
            builder.build();
            webView.loadUrl(builder.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        webView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CameraActivity.this, "reloading", Toast.LENGTH_SHORT);
                webView.reload();
            }
        });

        btnUp.setOnClickListener(this);
        btnDown.setOnClickListener(this);
        btnHome.setOnClickListener(this);
        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        HashMap<String, String> parametrs = new HashMap<String, String>();
        parametrs.put("action", "cam_mv");
        parametrs.put("lang", "eng");

        MovingCamera movingCamera;

        switch (view.getId()) {
            case R.id.btnHome:
                parametrs.put("direction", "cam_home");
                break;
            case R.id.btnLeft:
                parametrs.put("direction", "cam_left");
                break;
            case R.id.btnRight:
                parametrs.put("direction", "cam_right");
                break;
            case R.id.btnUp:
                parametrs.put("direction", "cam_up");
                break;
            case R.id.btnDown:
                parametrs.put("direction", "cam_down");
                break;
        }

        //TODO
//        movingCamera = new MovingCamera(mCamera.getUrl(),
//                LOGIN_PASSWORD, ACTION_SUFFIX, parametrs);
//
//        movingCamera.execute("");
//        try {
//            Boolean result = movingCamera.get();
//            Toast.makeText(this, result.toString(), Toast.LENGTH_SHORT).show();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }

    }
}
